import pytest

from main.models import MainVideo, Product


@pytest.fixture()
def test_user_creation(django_user_model):
    user = django_user_model.objects.create(
        username='test', password='password'
    )
    return user




@pytest.fixture()
def create_main_video():
    main_video = MainVideo.objects.create(main_video='https://www.youtube.com/embed/k0vt8sG2jgo')
    return main_video
