import pytest


from django.urls import reverse



@pytest.mark.django_db
def test_user(test_user_creation):
    assert test_user_creation.username == 'test'


@pytest.mark.django_db
def test_main_view(client):
    url = reverse('main')
    response = client.get(url)
    assert response.status_code == 200


@pytest.mark.django_db
def test_vibe_view(client):
    url = reverse('vibe-dance')
    response = client.get(url)
    assert response.status_code == 200


@pytest.mark.django_db
def test_shop_view(client):
    url = reverse('vibe-shop')
    response = client.get(url)
    assert response.status_code == 200
