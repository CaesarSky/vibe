from statistics import mode
from django.db import models


# Dance
from main.model_instance import OurProject


class MainVideo(models.Model):
    main_video = models.URLField(verbose_name='Видео на главной странице')

    class Meta:
        verbose_name = 'Главное видео'
        verbose_name_plural = 'Главное видео'
    
    def __str__(self) -> str:
        return 'Главное видео'


class Slide(models.Model):
    title = models.CharField(verbose_name='Наименование', max_length=255)
    text = models.CharField(verbose_name='Текст', max_length=255)
    image = models.ImageField(verbose_name='Фотография в карусели', upload_to='dance_slide_images')

    class Meta:
        verbose_name = 'Слайд'
        verbose_name_plural = 'Слайды'
    
    def __str__(self) -> str:
        return self.title


class Teacher(models.Model):
    image = models.ImageField(verbose_name='Фотография', upload_to='teachers_images')
    video_url = models.URLField(verbose_name='Ссылка на видео в ютубе')
    name = models.CharField(verbose_name='Имя преподавателя', max_length=255)
    text = models.TextField(verbose_name='Коротко о преподавателе')

    class Meta:
        verbose_name = 'Преподаватель'
        verbose_name_plural = 'Преподаватели'
    
    def __str__(self) -> str:
        return self.name


# Shop
class SlideShop(models.Model):
    image = models.ImageField(verbose_name='Фотография на слайде', upload_to='shops_slide')
    title = models.CharField(verbose_name='Наименование', max_length=255)

    class Meta:
        verbose_name = 'Слайд на shop'
        verbose_name_plural = 'Слайды на shop'
    
    def __str__(self) -> str:
        return self.title


class Product(models.Model):
    image = models.ImageField(verbose_name='Фотография товара', upload_to='products_images')
    new_price = models.DecimalField(
        verbose_name='Новая цена товара (или первоначальная)', 
        max_digits=10, decimal_places=2
    )
    old_price = models.DecimalField(
        verbose_name='Старая цена(после скидки или понижения цены у товара)',
        max_digits=10, decimal_places=2, blank=True, null=True
    )
    text = models.TextField(verbose_name='Краткое описание товара')
    title = models.CharField(verbose_name='Наименование товара', max_length=255, blank=True, null=True)

    class Meta:
        verbose_name = 'Продукт'
        verbose_name_plural = 'Продукты'
    
    def __str__(self) -> str:
        return self.title or self.text


# Prod
class ProductionSlide(models.Model):
    image = models.ImageField(verbose_name='Фотография', upload_to='production_slide')
    title = models.CharField(verbose_name='Описание', max_length=100)

    class Meta:
        verbose_name = 'Слайд'
        verbose_name_plural = 'Слайды'

    def __str__(self):
        return self.title


class ProductionServices(models.Model):
    text = models.CharField(verbose_name='Услуга', max_length=200)

    class Meta:
        verbose_name = 'Сервис'
        verbose_name_plural = 'Сервисы'

    def __str__(self):
        return self.text


class ProductionServiceImage(models.Model):
    image = models.ImageField(verbose_name='Фотография', upload_to='production_service_image')
    text = models.CharField(verbose_name='Описание', max_length=100)

    class Meta:
        verbose_name = 'Фотография сервиса'
        verbose_name_plural = 'Фотография сервиса'

    def __str__(self):
        return self.text


class ProductionPartners(models.Model):
    image = models.ImageField(verbose_name='Фотография', upload_to='production_partners')
    text = models.CharField(verbose_name='Описание', max_length=100)

    class Meta:
        verbose_name = 'Партнер'
        verbose_name_plural = 'Партнеры'

    def __str__(self):
        return self.text


class FirstImageProject(OurProject):
    class Meta:
        verbose_name = '1 Фотография проекта'
        verbose_name_plural = '1 Фотография проекта'


class SecondImageProject(OurProject):
    class Meta:
        verbose_name = '2 Фотография проекта'
        verbose_name_plural = '2 Фотография проекта'


class ThirdImageProject(OurProject):
    class Meta:
        verbose_name = '3 Фотография проекта'
        verbose_name_plural = '3 Фотография проекта'


class FourthImageProject(OurProject):
    class Meta:
        verbose_name = '4 Фотография проекта'
        verbose_name_plural = '4 Фотография проекта'


class FifthImageProject(OurProject):
    class Meta:
        verbose_name = '5 Фотография проекта'
        verbose_name_plural = '5 Фотография проекта'
