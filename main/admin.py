from django.contrib import admin

from .models import (
    MainVideo, Product, Slide, SlideShop, Teacher,
    ProductionSlide, ProductionPartners, ProductionServices,
    ProductionServiceImage, FifthImageProject,
    FirstImageProject, FourthImageProject,
    SecondImageProject, ThirdImageProject
)


class MainAdminPanel(admin.ModelAdmin):
    list_display = ('__str__', )


admin.site.register(FifthImageProject, MainAdminPanel)
admin.site.register(FourthImageProject, MainAdminPanel)
admin.site.register(ThirdImageProject, MainAdminPanel)
admin.site.register(SecondImageProject, MainAdminPanel)
admin.site.register(FirstImageProject, MainAdminPanel)
admin.site.register(ProductionServiceImage, MainAdminPanel)
admin.site.register(ProductionServices, MainAdminPanel)
admin.site.register(ProductionPartners, MainAdminPanel)
admin.site.register(ProductionSlide, MainAdminPanel)
admin.site.register(MainVideo, MainAdminPanel)
admin.site.register(Slide, MainAdminPanel)
admin.site.register(Teacher, MainAdminPanel)
admin.site.register(Product, MainAdminPanel)
admin.site.register(SlideShop, MainAdminPanel)
