from django.db import models


class OurProject(models.Model):
    image = models.ImageField(verbose_name='Фотография', upload_to='production_partners')
    text = models.CharField(verbose_name='Описание', max_length=100)

    def __str__(self):
        return self.text
