from django.urls import path

from .views import (
    MainPageView, VibeDanceTemplateView, VibeShopTemplateView,
    ProductionTemplateView
)

urlpatterns = [
    path('', MainPageView.as_view(), name='main'),
    path('dance/', VibeDanceTemplateView.as_view(), name='vibe-dance'),
    path('shop/', VibeShopTemplateView.as_view(), name='vibe-shop'),
    path('production/', ProductionTemplateView.as_view(), name='production')
]
