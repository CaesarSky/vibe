from typing import Any, Dict
from django.views.generic import TemplateView

from .models import (
    MainVideo, Product, Slide, SlideShop, Teacher,
    ProductionSlide, ProductionPartners, ProductionServices,
    ProductionServiceImage, FifthImageProject,
    FirstImageProject, FourthImageProject,
    SecondImageProject, ThirdImageProject
)


class MainPageView(TemplateView):
    template_name: str = 'home.html'


class ProductionTemplateView(TemplateView):
    template_name: str = 'production.html'

    def get_context_data(self, **kwargs: Any) -> Dict[str, Any]:
        context = super(ProductionTemplateView, self).get_context_data()
        context['slides'] = ProductionSlide.objects.all()
        context['partners'] = ProductionPartners.objects.all()
        context['services'] = ProductionServices.objects.all()
        context['services_image'] = ProductionServiceImage.objects.first()
        context['first_image'] = FirstImageProject.objects.first()
        context['second_image'] = SecondImageProject.objects.first()
        context['third_image'] = ThirdImageProject.objects.first()
        context['fourth_image'] = FourthImageProject.objects.first()
        context['fifth_image'] = FifthImageProject.objects.first()
        return context


class VibeDanceTemplateView(TemplateView):
    template_name: str = 'vibe_dance.html'

    def get_context_data(self, **kwargs: Any) -> Dict[str, Any]:
        context = super().get_context_data(**kwargs)
        context['main_video'] = MainVideo.objects.first()
        context['slides'] = Slide.objects.all()
        context['teachers'] = Teacher.objects.all()
        return context


class VibeShopTemplateView(TemplateView):
    template_name: str = 'vibe_shop.html'

    def get_context_data(self, **kwargs: Any) -> Dict[str, Any]:
        context = super().get_context_data(**kwargs)
        context['slides'] = SlideShop.objects.all()
        context['products'] = Product.objects.all()
        return context
