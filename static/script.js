$(document).ready(function () {
    // добавление карты
    var map;
    DG.then(function () {
        map = DG.map('map', {
            center: [42.874320, 74.610561],
            zoom: 17
        });
        DG.marker([42.874178, 74.611869]).addTo(map);
    });


    // настройки карусели vibe_dance
    $('.loop').owlCarousel({
        center: true,
        items:1.4,
        loop:true,
        margin:150,
        nav:true,
        dots: true,
        autoplay:true,
        autoplayTimeout:4000,
        autoplayHoverPause:true,
        autoplaySpeed: 1000,
        dotsContainer: '#carousel-custom-dots',
        navText: ["<img src='./images/arrow_left.png'>","<img src='./images/arrow_right.png'>"],
        responsive:{
            1200:{
                // items:1.4,
            },
            900:{
                margin:120,
                // items:1.4,
            },
            768:{
                margin:100,
                // items:1,
            },
            580:{
                nav:true,
                margin:50,
                // items:1,
            },
            0:{
                items:1,
                nav:false,
                margin:0,
            }
        }
    });
    // показать/скрыть меню

    $('.menu_image_wrapper').on('click', function(){
        $('.navbar_hidden').slideToggle( "slow", function() {
            $('.navbar_hidden_inner ').css( "display", "none" )
            $('.navbar_hidden_inner').css( "display", "flex" )
        });
    });

    // показать плеер
    $('.teachers_block_item').on('click', function(){
            $(this).addClass('active')
            $(this).find('.teachers_image_icon').remove()
    });
    // кастомные дотсы vibe dance
    $('.owl-dots').click(function () {
        owl.trigger('to.owl.carousel', [$(this).index(), 300]);
    });

    // замена header
    $(window).scroll(function() {
        var y = $(this).scrollTop();
        if (y > 0) {
            $('.header').addClass('header_scroll');
            $('.header_logo').addClass('header_logo_scroll');
            // $('.navbar_hidden').css( "top", "80px" )
        } else {
            $('.header').removeClass('header_scroll');
            $('.header_logo').removeClass('header_logo_scroll');
        }
    });

    // показать/скрыть список vibe_production
    let count = 3;
    let bodyWidth = $('body').width();
    if(bodyWidth < 768){
        count = 2;
    }
        $('.partners_item').each(function (index, item){
            if(index > count){
                $(item).addClass('partners_item_hidden')
            }
        })

    $('.show_all').on('click', function(){
        $('.show').toggleClass('show_active');
        $('.partners_item_hidden').slideToggle( "fast", function() {});
    })

    //карусель для vibe_production
    $('.production_slider').owlCarousel({
        center: true,
        items:1,
        loop:true,
        margin:0,
        nav:true,
        dots: true,
        autoplay:true,
        autoplayTimeout:4000,
        autoplayHoverPause:true,
        autoplaySpeed: 1000,
        dotsContainer: '#carousel-custom-dots',
        navText: ["<img src='/static/images/arrow_left.png'>","<img src='/static/images/arrow_right.png'>"],
        responsive:{
            580:{
                nav:true,
            },
            0:{
                nav:false,
            }
        }
    });
    //карусель для modal vibe_shop
    $('.modal_slider').owlCarousel({
        center: true,
        items:1,
        loop:true,
        nav: true,
        dots: false,
        // nav:true,
        navText: ["<img src='/static/images/arrow_left.png'>","<img src='/static/images/arrow_right.png'>"],
        responsive:{
            580:{
                dots:false,
            },
            0:{
                dots:true,
            }
        }
    });
    // кастомные дотсы vibe production
    $('.owl-dots').click(function () {
        owl.trigger('to.owl.carousel', [$(this).index(), 300]);
    });

    // форма vibe production
    $("#field_name").blur(function() {
        if($(this).val() == null || $(this).val() == undefined || $(this).val() == "")
        {
           $('.error_field_name').css('display', 'block')
        }else{
            $('.error_field_name').css('display', 'none')
        }
    });

    $("#field_numb").blur(function() {
        const number = new RegExp( '^\\(?\\+([9]{2}?[6])\\)?[-. ]?([0-9]{3})[-. ]?([0-9]{3})[-. ]?([0-9]{3})$')
        if(number.test(this.value)){
            $('.error_field_numb').css('display', 'none')
        }else{
            $('.error_field_numb').css('display', 'block')
        }
    });

    $('#submitButton').click( function(e) {
        // тут запрос
        e.preventDefault();
        if( $("#field_name").val() !== "" && $("#field_numb").val() !== ""){
            $.ajax({
                url: 'some-url',
                type: 'post',
                dataType: 'json',
                data: $('form#form').serialize(),
                success: function(data) {
                    // ... do something with the data...
                }
            });
        }

    });

    //событие для поиска тут
    $('#search_input').keypress(function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
            alert('Был запрос на поиск')
        }
    });

    // $(this).addClass('active')

    //код модального окна vibe_shop

    var $modalOverlay = $('.modalOverlay'); // Cache your selectors
    var $modal        = $('.modal');
    var $modalClose   = $('.modalClose');
    var $modalOpen    = $('.modalOpen');

    $modalOpen.click(function(){
        $(this).find('.modal').fadeTo(500,1, function (){
            $(this).addClass('modal_active')
        });
        $(this).find('.modalOverlay').stop().fadeTo(500,1, function (){
        });

    });

    $modalOverlay.click(function(){
        $modalOverlay.stop().fadeTo(500,0, function(){ $(this).hide()});
    });

    $modal.click(function( e ) {
        e.stopPropagation(); // otherwise the click would bubble up to the overlay
    });

    $modalClose.click(function(){
        // $modalOverlay.click(); // simulate click to close modal
        $modalOverlay.stop().fadeTo(500,0, function(){ $(this).hide()});
    });

});
